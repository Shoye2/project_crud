-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2018 at 02:10 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mymen_mohanogor_college`
--

-- --------------------------------------------------------

--
-- Table structure for table `college_female_std_info`
--

CREATE TABLE `college_female_std_info` (
  `id` int(11) NOT NULL,
  `Name` varchar(32) NOT NULL,
  `Email` varchar(32) NOT NULL,
  `Gender` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `college_female_std_info`
--

INSERT INTO `college_female_std_info` (`id`, `Name`, `Email`, `Gender`) VALUES
(6, 'shoyeb', 'hasan@gmail.com', 'Male'),
(7, 'Rubi', 'rubi@gmail.com', 'Female'),
(197, 'kabir', 'sdf@gmail.com', 'Male'),
(202, 'sapna', 'trivuz2010s@gmail.com', 'Male');

-- --------------------------------------------------------

--
-- Table structure for table `college_male_std_info`
--

CREATE TABLE `college_male_std_info` (
  `Serial no.` int(11) NOT NULL,
  `Name` varchar(64) NOT NULL,
  `Email` varchar(64) NOT NULL,
  `Gender` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `college_male_std_info`
--

INSERT INTO `college_male_std_info` (`Serial no.`, `Name`, `Email`, `Gender`) VALUES
(1, 'amitumi', 'as@gmail.com', 'male'),
(2, 'asdee', 'asde@gmail.com', 'male'),
(3, 'wwwwwwwww', 'zaman@gmail.com', 'male'),
(4, 'ashraf', 'moniar@gmail.com', 'male');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `college_female_std_info`
--
ALTER TABLE `college_female_std_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `college_male_std_info`
--
ALTER TABLE `college_male_std_info`
  ADD PRIMARY KEY (`Serial no.`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `college_female_std_info`
--
ALTER TABLE `college_female_std_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;

--
-- AUTO_INCREMENT for table `college_male_std_info`
--
ALTER TABLE `college_male_std_info`
  MODIFY `Serial no.` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
